# jdig - The Super tool for a Hosting Support Team

#Author : Jobin Joseph  
#JobinJoseph.com  
#Nixhive.com  
#@NixJobin  

--- Will update soon ---

[Download Now](https://github.com/nixjobin/jdig/releases)

## Usage
    jdig example.com  
    jdig example.com 8.8.8.8  #use 8.8.8.8 as DNS server for resolution
    jdig example.com dns1.example.com  #use dns1.example.com as DNS server for resolution
  
